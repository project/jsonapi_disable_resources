<?php

/**
 * @file
 * Drupal hooks.
 */

/**
 * Alter ResourceTypeBuildEventSubscriber::getAllowedResourceTypeNameFields.
 *
 * @param array $allowed_resource_type_name_fields
 *   The fields to alter.
 */
function hook_alter_jsonapi_disable_resources_allowed_resource_type_name_fields(array &$allowed_resource_type_name_fields) {

}
