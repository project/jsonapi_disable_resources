<?php

namespace Drupal\jsonapi_disable_resources\EventSubscriber;

use Drupal\jsonapi\ResourceType\ResourceTypeBuildEvents;
use Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ResourceTypeBuildEventSubscriber.
 *
 * Eventsubscriber which disables unwanted JSONAPI resources.
 *
 * @package Drupal\jsonapi_disable_resources\EventSubscriber
 */
class ResourceTypeBuildEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ResourceTypeBuildEvents::BUILD => [
        ['disableResourceType'],
      ],
    ];
  }

  /**
   * Disables unwanted resource types.
   *
   * @param \Drupal\jsonapi\ResourceType\ResourceTypeBuildEvent $event
   *   The build event.
   */
  public function disableResourceType(ResourceTypeBuildEvent $event) {
    $allowed_resource_type_name_fields = self::getAllowedResourceTypeNameFields();

    $resource_type_name = $event->getResourceTypeName();

    if (!isset($allowed_resource_type_name_fields[$resource_type_name])) {
      $event->disableResourceType();
    }
    else {
      // Resource allowed, filter allowed fields.
      foreach ($event->getFields() as $field_name => $field) {
        if ($field_name === 'uuid') {
          // The id/uuid fields is always allowed.
          continue;
        }
        if (!in_array($field_name, $allowed_resource_type_name_fields[$resource_type_name])) {
          $event->disableField($field);
        }
      }
    }
  }

  /**
   * Get a list of allowed resource type names and their fields.
   *
   * Use hook_alter_jsonapi_disable_resources_allowed_resource_type_name_fields
   * to alter the values returned by this function.
   *
   * @return string[][]
   *   The list.
   */
  public static function getAllowedResourceTypeNameFields(): array {
    $allowed_resource_type_name_fields = [];

    \Drupal::moduleHandler()
      ->alter('jsonapi_disable_resources_allowed_resource_type_name_fields', $allowed_resource_type_name_fields);

    return $allowed_resource_type_name_fields;
  }

}
