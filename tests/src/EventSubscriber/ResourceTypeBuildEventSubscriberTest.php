<?php

namespace Drupal\Tests\jsonapi_disable_resources\EventSubscriber;

use Drupal\jsonapi_disable_resources\EventSubscriber\ResourceTypeBuildEventSubscriber;
use Drupal\Tests\BrowserTestBase;

/**
 * Class ResourceTypeBuildEventSubscriberTest.
 *
 * Test the ResourceTypeBuildEventSubscriber class.
 *
 * @package Drupal\Tests\jsonapi_disable_resources\EventSubscriber
 * @group jsonapi_disable_resources
 */
class ResourceTypeBuildEventSubscriberTest extends BrowserTestBase {

  /**
   * Enabled modules.
   *
   * The user and block modules are enabled to test the functionality.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'block',
    'jsonapi',
    'jsonapi_disable_resources',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * TestEnabledJsonapiResources.
   */
  public function testEnabledJsonapiResources(): void {
    $this->drupalGet('');

    /**
     * @var \Drupal\Core\Routing\RouteProviderInterface $route_provider
     */
    $route_provider = $this->container->get('router.route_provider');

    $allowed_resource_type_name_fields = ResourceTypeBuildEventSubscriber::getAllowedResourceTypeNameFields();
    $allowed_resource_type_path_fields = [];

    foreach ($allowed_resource_type_name_fields as $resource_type => $fields) {
      $path = '/jsonapi/' . str_replace('--', '/', $resource_type);
      $allowed_resource_type_path_fields[$path] = $fields;
    }

    foreach ($route_provider->getAllRoutes() as $route) {

      $allowed = FALSE;
      $path = $route->getPath();

      if (stripos($path, '/jsonapi/') !== 0) {
        // Only check /jsonapi/ routes.
        continue;
      }

      // Check if path is allowed.
      $path = '';
      $fields = [];
      foreach ($allowed_resource_type_path_fields as $path => $fields) {
        if (stripos($path, $path) === 0) {
          $allowed = TRUE;
          break;
        }
      }

      if (empty($fields)) {
        $this->fail('$fields is empty for ' . $path);
      }

      // Check allowed fields.
      $this->drupalGet(ltrim($path, '/'));
      $json = json_decode($this->getSession()->getPage()->getContent(), TRUE);

      foreach ($json['data'] as $result) {
        if (!isset($result['attributes'])) {
          continue;
        }

        foreach ($result['attributes'] as $key => $value) {
          if (!in_array($key, $fields)) {
            $this->fail('Resource ' . $path . ' has attribute ' . $key . ' which is not allowed.');
          }
        }
      }

      if (!$allowed) {
        $this->fail('Route ' . $path . ' should not be allowed.');
      }
    }
  }

}
